// Theory in Readme file

// 1
const arr = ["travel", "hello", "eat", "ski", "lift"];
function length(arr) {
  let counter = 0;
  for (let index = 0; index < arr.length; index++) {
    if(arr[index].length > 3){
      counter++;
    } 
  }
  console.log(counter); 
}
length(arr);

//2

const users = [
  {   
    name: "Іван",
    age: 25,
    sex: "чоловіча"
  },
  { 
    name: "Катя",
    age: 22,
    sex: "жіноча"
  },
  { 
    name: "Леся",
    age: 18,
    sex: "жіноча"
  },
  {   
    name: "Олександр",
    age: 27,
    sex: "чоловіча"
  }
];


let filtered = users.filter(user => user.sex === 'чоловіча');

console.log(filtered);
  

//3
const array = ['hello', 'world', 23, '23', null];
const userType = prompt('Enter type:').toLowerCase();

function filterBy(arr, type) {
  let newArray = [];
  for (let i = 0, j = 0; i < arr.length; i++) {
    if(typeof(arr[i]) === type){
      newArray[j] = arr[i];
      j++;
    }
  }
  console.log(newArray);
}

filterBy(array, userType);
