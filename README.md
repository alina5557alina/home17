# home17

Теоретичні питання
1. Опишіть своїми словами як працює метод forEach.
Перебирає всі елементи масиву, також може виконати певні дії з усими елементами даного масиву.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
Мутуючі
push

let fruits = ['apple', 'banana', 'orange'];
fruits.push('pineapple');
// ['apple', 'banana', 'orange', 'pineapple']

unshift
pop
shift
splice

Немутуючі
slice
concat

let fruits = ['apple', 'banana', 'orange'];
let vegetables = ['potato', 'tomato', 'cucumber'];
let all = fruits.concat(vegetables);
// ['apple', 'banana', 'orange', 'potato', 'tomato', 'cucumber']

map
filter
reduce
sort
reverse
every
some
includes
find
indexOf
findIndex

3. Як можна перевірити, що та чи інша змінна є масивом?
Array.isArray()

4. В яких випадках краще використовувати метод map(), а в яких forEach()?
map() краще використовувати у випадках коли потрібно внести зміни у вихідний масив 
та створити новий на основі змін.

forEach потрібно використовувати коли зміни потрібно залишити у вихідному масиві
